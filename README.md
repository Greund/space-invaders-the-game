# README #

Tambahan Spek tolong beri tanda "\*\*" untuk memudahkan pembacaan

## Space Invaders: The Game ##
Game space invaders yang di impelementasikan menggunakan dua buah layar LED 16x32 dan sebuah piezo buzzer untuk efek suara.
Berikut adalah spesifikasi dari game ini:

1. Hanya memiliki satu level saja
2. Menampilkan skor permainan saat ini(apakah perlu dapat menuliskan nama seperti di arcade?)
3. Mencatat 10 skor terbaik hingga saat ini
4. Tidak memiliki batas waktu dan permainan akan terus berlangsung hingga pemain mati atau musuh habis
5. Terdapat 3 jenis musuh (diam, bergerak, terbang)
6. Terdapat 3 tingkat kesulitan yang mempengaruhi jumlah dan jenis musuh
	- Mudah(Easy) terdiri dari musuh diam sebanyak 8x5
	- Biasa(Normal) terdiri dari musuh diam dan bergerak sebanyak 8x6
	- Sulit terdiri dari musuh diam, bergerak, dan terbang sebanyak 8x7
7. Efek suara terdiri dari:
	- Pemain menembak
	- Pemain mati
	- Musuh mati
	- Musuh terbang
	- Musuh menembak
	- Permainan dimulai
	- Permainan berakhir
	- Title Screen

## Spesifikasi alat ##
- Layar LED 16x32 (sudah tersedia di lab)  
- Arduino UNO/Mega 2560  
- Piezo Buzzer (perlu beli)  
- Jumper M-M  maksimal 16  
- Button x3  
- Breadboard  
- Extra (Casing, pembungkus kabel, dkk)
