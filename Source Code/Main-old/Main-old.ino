
//Desain tampilan : http://pixelartmaker.com/art/fcdcce80c6f75c0

// Arduino FreeRTOS Library
#include <Arduino_FreeRTOS.h>

// Library for DMD Screen
#include <SPI.h>
#include <DMD2.h>

// Functions needed for RNG (enemy shoot proc)
#include <time.h> // time() for random seed
#include <stdlib.h> // srand() and rand() to generate random number

// Daftar frekuensi nada
#define NOTE_B0  31
#define NOTE_C1  33
#define NOTE_CS1 35
#define NOTE_D1  37
#define NOTE_DS1 39
#define NOTE_E1  41
#define NOTE_F1  44
#define NOTE_FS1 46
#define NOTE_G1  49
#define NOTE_GS1 52
#define NOTE_A1  55
#define NOTE_AS1 58
#define NOTE_B1  62
#define NOTE_C2  65
#define NOTE_CS2 69
#define NOTE_D2  73
#define NOTE_DS2 78
#define NOTE_E2  82
#define NOTE_F2  87
#define NOTE_FS2 93
#define NOTE_G2  98
#define NOTE_GS2 104
#define NOTE_A2  110
#define NOTE_AS2 117
#define NOTE_B2  123
#define NOTE_C3  131
#define NOTE_CS3 139
#define NOTE_D3  147
#define NOTE_DS3 156
#define NOTE_E3  165
#define NOTE_F3  175
#define NOTE_FS3 185
#define NOTE_G3  196
#define NOTE_GS3 208
#define NOTE_A3  220
#define NOTE_AS3 233
#define NOTE_B3  247
#define NOTE_C4  262
#define NOTE_CS4 277
#define NOTE_D4  294
#define NOTE_DS4 311
#define NOTE_E4  330
#define NOTE_F4  349
#define NOTE_FS4 370
#define NOTE_G4  392
#define NOTE_GS4 415
#define NOTE_A4  440
#define NOTE_AS4 466
#define NOTE_B4  494
#define NOTE_C5  523
#define NOTE_CS5 554
#define NOTE_D5  587
#define NOTE_DS5 622
#define NOTE_E5  659
#define NOTE_F5  698
#define NOTE_FS5 740
#define NOTE_G5  784
#define NOTE_GS5 831
#define NOTE_A5  880
#define NOTE_AS5 932
#define NOTE_B5  988
#define NOTE_C6  1047
#define NOTE_CS6 1109
#define NOTE_D6  1175
#define NOTE_DS6 1245
#define NOTE_E6  1319
#define NOTE_F6  1397
#define NOTE_FS6 1480
#define NOTE_G6  1568
#define NOTE_GS6 1661
#define NOTE_A6  1760
#define NOTE_AS6 1865
#define NOTE_B6  1976
#define NOTE_C7  2093
#define NOTE_CS7 2217
#define NOTE_D7  2349
#define NOTE_DS7 2489
#define NOTE_E7  2637
#define NOTE_F7  2794
#define NOTE_FS7 2960
#define NOTE_G7  3136
#define NOTE_GS7 3322
#define NOTE_A7  3520
#define NOTE_AS7 3729
#define NOTE_B7  3951
#define NOTE_C8  4186
#define NOTE_CS8 4435
#define NOTE_D8  4699
#define NOTE_DS8 4978

// Pin GPIO
#define r_button 2 // Move right
#define l_button 3 // Move left
#define m_button 4 // Confirm, shoot
#define melodyPin 5 // Pin buzzer

// Define types
// Player and enemy type
typedef struct
{
	int type; // Enemy types
	int x; // X position
	int y; // Y position
	int hp; // Hit points
} Ship;

// Projectile type
typedef struct
{
	int hostile; // 1 = hostile (shot by the enemy), 0 = non-hostile (shot by the player)
	int x; // X position
	int y; // Y position
	int valid; // 0 = expired (hit an enemy/player or out of bounds)
} Projectile;

// Define tasks
void TaskGame(void *pvParameters);
void TaskDisplay(void *pvParameters);
void TaskButton(void *pvParameters);
void TaskBGM(void *pvParameter);
void TaskSFX(void *pvParameter);

SoftDMD dmd(1,1);  // DMD that controls the entire display

// Global variables
int move_input; //right = 1, left = -1, no move = 0
int shoot_input; //shoot = 1, no shoot = 0

int gameScreen = 0; // 0 = Title, 1 = stage select, 2 = ingame, 3 = result
int cursorPos = 1; // Stage select cursor position range 1 - 3
int victory = 0; // Victory indicator

int song_flag; // 1=shoot, 2=hit enemy, 3=hit player, 4=enemy destroyed, 5=player destroyed

 // Underworld melody
int title_melody[] = {
	NOTE_F4,  NOTE_F4, NOTE_F4,
	NOTE_AS4, NOTE_F5, NOTE_DS5,  NOTE_D5,
	NOTE_C5, NOTE_AS5, NOTE_F5, NOTE_DS5,
	NOTE_D5,  NOTE_C5, NOTE_AS5, NOTE_F5,
	NOTE_DS5, NOTE_D5, NOTE_DS5,   NOTE_C5
};

//Underwolrd tempo
int title_tempo[] = {
	6, 6, 6, 2,
	2, 6, 6, 6,
	2, 4, 6, 6,  
	6, 2, 4, 6, 
	6, 6, 2
};

int ingame_melody[] = {  
	NOTE_A4, NOTE_A4,
	NOTE_A4, NOTE_F4,
	NOTE_C5, NOTE_A4,
	NOTE_F4, NOTE_C5, NOTE_A4,
	NOTE_E5, NOTE_E5,
	NOTE_E5, NOTE_F5,  NOTE_C5,
	NOTE_G5, NOTE_F5,
	NOTE_C5, NOTE_A4
};

int ingame_tempo[]  = { 
	4, 4,
	4, 6,
	8, 4,
	5, 8, 2,
	4, 4,
	4, 5, 12,
	4, 5,
	12, 2
};

Ship enemyArray[12]; // Array containing enemy data
Ship playerData; // Player data
Projectile projArray[20]; // Array containing all projectile data

void setup() {
	// Set button input as pulled-up
	pinMode(r_button, INPUT_PULLUP);
	pinMode(l_button, INPUT_PULLUP);
	pinMode(m_button, INPUT_PULLUP);
	pinMode(melodyPin, OUTPUT);
	// Initialize serial communication at 9600 bits per second
	Serial.begin(9600);
	Serial.println("begin");
  
	// Initialize DMD
	dmd.setBrightness(255);
	dmd.beginNoTimer();
	
	srand((unsigned int)time(NULL)); // Random function seed

	// Create RTOS tasks
	Serial.println("create");
	xTaskCreate(TaskDisplay, (const portCHAR *)"Display", 128, NULL, 1, NULL);
	xTaskCreate(TaskGame, (const portCHAR *)"Game", 128, NULL, 1, NULL);
	xTaskCreate(TaskButton,(const portCHAR *)"Button", 128, NULL, 1, NULL);
	// xTaskCreate(TaskBGM,(const portCHAR *)"BGM", 128, NULL, 1, NULL);
	// xTaskCreate(TaskSFX,(const portCHAR *)"SFX", 128, NULL, 1, NULL);
	Serial.println("sch");
	
	// Now the task scheduler, which takes over control of scheduling individual tasks, is automatically started.
}

void loop()
{
	// Empty. Things are done in Tasks.
}

/*--------------------------------------------------*/
/*---------------------- Tasks ---------------------*/
/*--------------------------------------------------*/

// Task untuk membaca input button, dan membaca hanya ketika button dilepas
void TaskButton(void *pvParameter)
{
	TickType_t xLastWakeTime;
	uint8_t left, right, mid;
	static int move_input_lock, shoot_input_lock;
	xLastWakeTime = xTaskGetTickCount();
	for( ; ; )
	{	
		// Serial.println("TaskButton..");
		// Membaca input tombol ke variabel
		left = digitalRead(l_button);
		right = digitalRead(r_button);
		mid = digitalRead(m_button);

		// Right button untuk bergerak ke kanan
		if(right == 0)
		{
			if(move_input_lock == 0)
			{
				move_input = 1;
				move_input_lock = 1;
			}
			else
				move_input = 0;
		}
		
		//Left button untuk bergerak ke kiri
		else if(left == 0)
		{
			if(move_input_lock == 0)
			{
				move_input = -1;
				move_input_lock = 1;
			}
			else
				move_input = 0;
		}
		else
		{
			move_input = 0;
			move_input_lock = 0;
		}
		
		// Mid button untuk menembak atau memilih
		if(mid == 0)
		{
			if(shoot_input_lock == 0)
			{
				shoot_input = 1;
				shoot_input_lock = 1;
			}
			else
				shoot_input = 0;
		}
		else
		{
			shoot_input = 0;
			shoot_input_lock = 0;
		}

		//Delay 90ms untuk frekuensi tombol 11Hz
		vTaskDelayUntil( &xLastWakeTime, pdMS_TO_TICKS(90) );
	}
}

// Task untuk operasi menu dan game (menggerakkan player, musuh, dan projectile)
void TaskGame(void *pvParameters)
{
	(void) pvParameters;
	TickType_t xLastWakeTime;
	int i; // Counter for looping the data array
	int counter; // Counter for enemy animation/move pattern
	
	xLastWakeTime = xTaskGetTickCount();
	for(;;)
	{	
		// Serial.println("TaskGame..");
		
		// Title screen
		if(gameScreen == 0)
		{
			// Waits for user input to go to stage selection screen
			if(move_input || shoot_input)
			{
				gameScreen = 1;
				
				//SFX
				song_flag=1;
			}
		}
		
		// Stage selection screen
		else if(gameScreen == 1)
		{
			// Move cursor
			cursorPos += move_input;
			if(cursorPos < 1)
				cursorPos = 1;
			if(cursorPos > 3)
				cursorPos = 3;
			
			// Confirm stage
			if(shoot_input)
			{
				//SFX
				song_flag=1;
				
				// Create player
				playerData.type = 0;
				playerData.hp = 3;
				playerData.x = 16;
				playerData.y = 15;
				
				counter = -15; // Initialize counter
				
				// Clear all projectile (if any)
				for(i=0; i<20; i++)
					projArray[i].valid = 0;
				
				// Clear all enemy (if any)
				for(i=0; i<12; i++)
					enemyArray[i].hp = 0;
				
				// Generate stage according to difficulty selected
				generateStage(cursorPos);
				gameScreen = 2;
				
				//SFX
				song_flag=1;
			}
		}
		
		// Ingame screen
		else if(gameScreen == 2)
		{
			// Move player
			playerData.x += move_input;
			
			// Player shoot
			if(shoot_input)
			{
				//SFX
				song_flag=1;
				
				for(i=0; i<20; i++)
				{
					// Looks for empty location in array
					if(projArray[i].valid == 0)
					{
						// Create projectile
						projArray[i].hostile = 0;
						projArray[i].x = playerData.x;
						projArray[i].y = (playerData.y)-2;
						projArray[i].valid = 1;
						break;
					}
				}
			}
			
			// Move all projectile
			moveProjectile();
			
			// Check for player death
			if(playerData.hp == 0)
			{
				//SFX
				song_flag=5;
				
				// Go to game over screen
				gameScreen = 3;
				victory = 0;
			}
			else
			{
				// Act all enemy (move and shoot proc)
				if(actEnemy(counter) == 0)
				{
					//SFX victory fanfare
          
					// Go to victory screen if there's no enemy remaining
					gameScreen = 3;
					victory = 1;
				}
			}
			
			// Serial.println(counter);
			counter++;
			if(counter > 71)
				counter = 0;
		}
		
		// Results screen
		else if(gameScreen == 3)
		{	
			if(shoot_input)
				gameScreen = 0;
		}

		// Periode task 90 ms
		vTaskDelayUntil( &xLastWakeTime, pdMS_TO_TICKS(90) );
	}
}

// Task untuk menampilkan tampilan pada layar DMD
void TaskDisplay(void *pvParameters)
{
	(void) pvParameters;
	TickType_t xLastWakeTime;
	int counter = 0; // Counter untuk animasi victory screen dan game over screen
	
	xLastWakeTime = xTaskGetTickCount();
	for(;;)
	{
		// Serial.println("TaskDisplay..");
		dmd.clearScreen(); // Kosongkan layar
		if(gameScreen == 0)
		{
			// Display title screen
			titleScreen();
		}
		else if(gameScreen == 1)
		{
			// Display stage selection screen
			showCursor(cursorPos);
			
			if((cursorPos == 1) || (cursorPos == 2))
				stageSelect1();
			
			else if(cursorPos == 3)
				stageSelect2();
		}
		else if(gameScreen == 2)
		{
			// Display ingame screen
			showPlayer();
			showEnemy();
			showProjectile();
		}
		else if(gameScreen == 3)
		{
			//Display results screen
			if(victory == 1)
				showVictory(counter/500);
			
			else
				showGameOver(counter/500);
		}
		
		counter++;
		if(counter == 1000)
			counter = 0;

		// Scanning layar
		dmd.scanDisplay();
		// Periode task 15 ms
		vTaskDelayUntil( &xLastWakeTime, pdMS_TO_TICKS(15) );
	}
}

void TaskBGM(void *pvParameter)
{
	TickType_t xLastWakeTime;
	uint8_t thisNoteBGM = 0;
	int noteDurBGM;
	int pauseBetweenNotes = 0;
	int len;
	int pauseCount = 0;
	
	xLastWakeTime = xTaskGetTickCount();
	for( ;; )
	{
		if(pauseCount >= pauseBetweenNotes)
		{
			if(gameScreen == 0)
			{
				thisNoteBGM = 0;
				len = sizeof(title_melody) / sizeof(int);
				while((thisNoteBGM<=len)&&(gameScreen==0)) 
				{
					if(thisNoteBGM<len)
					{
						// to calculate the note duration, take one second
						// divided by the note type.
						//e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
						noteDurBGM = 1000 / title_tempo[thisNoteBGM];
						taskENTER_CRITICAL();
						buzz(melodyPin, title_melody[thisNoteBGM], noteDurBGM);
						taskEXIT_CRITICAL();
						
						// to distinguish the notes, set a minimum time between them.
						// the note's duration + 30% seems to work well:
						pauseBetweenNotes = ( noteDurBGM*1.3 )/15;
						
						//Delay
						for(pauseCount=0; pauseCount<pauseBetweenNotes; pauseCount++)
							vTaskDelayUntil( &xLastWakeTime, pdMS_TO_TICKS(30) );
						
						// stop the tone playing:
						taskENTER_CRITICAL();
						buzz(melodyPin, 0, noteDurBGM);
						taskEXIT_CRITICAL();
						thisNoteBGM++;
					}
					
					else
					{
						thisNoteBGM=0;
					}		
				}		
			}
			
			else if(gameScreen==2) // BGM tergantung gameScreen
			{ 
				thisNoteBGM=0;
				len = sizeof(ingame_melody) / sizeof(int);
				
				while((thisNoteBGM<=len)&&(gameScreen==2)) 
				{
					if(thisNoteBGM<len)
					{
						// to calculate the note duration, take one second
						// divided by the note type.
						//e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
						noteDurBGM = 1000 / ingame_tempo[thisNoteBGM];
						taskENTER_CRITICAL();

						buzz(melodyPin, ingame_melody[thisNoteBGM], noteDurBGM);
						taskEXIT_CRITICAL();
						// to distinguish the notes, set a minimum time between them.
						// the note's duration + 30% seems to work well:
						pauseBetweenNotes =( noteDurBGM * 1.30)/15;
												
						// stop the tone playing:
						taskENTER_CRITICAL();
						buzz(melodyPin, 0, noteDurBGM);
						taskEXIT_CRITICAL();
						thisNoteBGM++;
					}
					else
					{
						thisNoteBGM=0;
					}
				}
			}
			pauseCount = 0;
		}
		
		pauseCount++;
		vTaskDelayUntil( &xLastWakeTime, pdMS_TO_TICKS(30) );
	}
}

 
 void TaskSFX(void *pvParameter)
 {
    TickType_t xLastWakeTime;
    uint8_t thisNote=0;//
     uint8_t len;
    int noteDur=0;
    
     xLastWakeTime = xTaskGetTickCount();
     for( ;; )
     {
        if(song_flag==1)//shoot
    {
      // to calculate the note duration, take one second
      // divided by the note type.
      //e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
      noteDur = 1000 / 8;
      taskENTER_CRITICAL();
      
     buzz(melodyPin, NOTE_E7, noteDur);
     taskEXIT_CRITICAL();
      song_flag=0;
            
    }
    else if(song_flag==2)//hit enemy
    {
      // to calculate the note duration, take one second
      // divided by the note type.
      //e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
      noteDur = 1000 / 8;
      taskENTER_CRITICAL();
      
      buzz(melodyPin, NOTE_D5, noteDur);
      taskEXIT_CRITICAL();
      song_flag=0;
    }
    else if(song_flag==3)//hit player
    {
      // to calculate the note duration, take one second
      // divided by the note type.
      //e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
      noteDur = 1000 / 8;
      taskENTER_CRITICAL();
      
      buzz(melodyPin, NOTE_F5, noteDur);
      taskEXIT_CRITICAL();
      song_flag=0;
    }
    else if(song_flag==4)//enemy destroyed
    {
      len=1;
      // to calculate the note duration, take one second
      // divided by the note type.
      //e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
      noteDur = 1000 / 4;
      taskENTER_CRITICAL();
      
    buzz(melodyPin, NOTE_G7, noteDur);
      taskEXIT_CRITICAL();
      if(thisNote<len)
      {
        thisNote++;
      }
      else
      {
        thisNote=0;
        song_flag=0;
      }
    }
    else if(song_flag==5)//enemy destroyed
    {
      len=1;
      // to calculate the note duration, take one second
      // divided by the note type.
      //e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
      noteDur = 1000 / 4;
      taskENTER_CRITICAL();
      
    buzz(melodyPin, NOTE_F7, noteDur);
      taskEXIT_CRITICAL();
      if(thisNote<len)
      {
        thisNote++;
      }
      else
      {
        thisNote=0;
        song_flag=0;
      }
    }
    int pauseBetweenNotes = ( noteDur* 1.30)/20;
      //delay
        for(int pauseCount=0;pauseCount<=pauseBetweenNotes;pauseCount++)
                {
                  vTaskDelayUntil( &xLastWakeTime, pdMS_TO_TICKS(25) );
                }
                // stop the tone playing:
               
     }
 }

/*------------------------------------------------------*/
/*---------------------- Functions ---------------------*/
/*------------------------------------------------------*/

// Fungsi untuk menampilkan title screen
void titleScreen()
{
	dmd.drawLine(1,1,3,1);
	dmd.drawLine(1,2,3,4);
	dmd.drawLine(1,5,3,5);

	dmd.drawLine(5,1,5,5);
	dmd.setPixel(6,1);
	dmd.setPixel(6,3);
	dmd.drawLine(7,1,7,3);

	dmd.drawLine(9,1,9,5);
	dmd.setPixel(10,1);
	dmd.setPixel(10,3);
	dmd.drawLine(11,1,11,5);
	
	dmd.drawLine(13,1,13,5);
	dmd.drawLine(14,1,15,1);
	dmd.drawLine(14,5,15,5);
	
	dmd.drawLine(17,1,17,5);
	dmd.drawLine(18,1,19,1);
	dmd.drawLine(18,3,19,3);
	dmd.drawLine(18,5,19,5);
	
	dmd.drawLine(3,7,5,7);
	dmd.drawLine(4,8,4,10);
	dmd.drawLine(3,11,5,11);
	
	dmd.drawLine(7,7,7,11);
	dmd.setPixel(8,7);
	dmd.drawLine(9,7,9,11);
	
	dmd.drawLine(11,7,11,10);
	dmd.drawLine(12,10,12,11);
	dmd.drawLine(13,7,13,10);
	
	dmd.drawLine(15,7,15,11);
	dmd.setPixel(16,7);
	dmd.setPixel(16,9);
	dmd.drawLine(17,7,17,11);
	
	dmd.drawLine(19,7,19,11);
	dmd.setPixel(20,7);
	dmd.setPixel(20,11);
	dmd.drawLine(21,8,21,10);
	
	dmd.drawLine(23,7,23,11);
	dmd.drawLine(24,7,25,7);
	dmd.drawLine(24,9,25,9);
	dmd.drawLine(24,11,25,11);
	
	dmd.drawLine(27,7,27,11);
	dmd.setPixel(28,7);
	dmd.setPixel(29,8);
	dmd.setPixel(28,9);
	dmd.drawLine(29,10,29,11);
}

// Fungsi untuk menampilkan pilihan stage easy dan normal
void stageSelect1()
{
	dmd.drawLine(5,1,5,5);
	dmd.drawLine(6,1,7,1);
	dmd.drawLine(6,3,7,3);
	dmd.drawLine(6,5,7,5);
	
	dmd.drawLine(9,1,9,5);
	dmd.setPixel(10,1);
	dmd.setPixel(10,3);
	dmd.drawLine(11,1,11,5);
	
	dmd.drawLine(13,1,15,1);
	dmd.drawLine(13,2,15,4);
	dmd.drawLine(13,5,15,5);
	
	dmd.drawLine(17,1,17,2);
	dmd.drawLine(19,1,19,2);
	dmd.drawLine(18,3,18,5);
	
	dmd.drawLine(5,8,5,12);
	dmd.setPixel(6,8);
	dmd.drawLine(7,8,7,12);
	
	dmd.drawLine(9,8,9,12);
	dmd.setPixel(10,8);
	dmd.setPixel(10,12);
	dmd.drawLine(11,8,11,12);
	
	dmd.drawLine(13,8,13,12);
	dmd.setPixel(14,8);
	dmd.setPixel(15,9);
	dmd.setPixel(14,10);
	dmd.drawLine(15,11,15,12);
	
	dmd.drawLine(17,8,17,12);
	dmd.setPixel(18,8);
	dmd.drawLine(19,9,19,12);
	dmd.setPixel(20,8);
	dmd.drawLine(21,8,21,12);
	
	dmd.drawLine(23,8,23,12);
	dmd.setPixel(24,8);
	dmd.setPixel(24,10);
	dmd.drawLine(25,8,25,12);
	
	dmd.drawLine(27,8,27,12);
	dmd.drawLine(28,12,29,12);
}

// Fungsi untuk menampilkan pilihan stage hard
void stageSelect2()
{
	dmd.drawLine(5,1,5,5);
	dmd.setPixel(6,3);
	dmd.drawLine(7,1,7,5);
	
	dmd.drawLine(9,1,9,5);
	dmd.setPixel(10,1);
	dmd.setPixel(10,3);
	dmd.drawLine(11,1,11,5);
	
	dmd.drawLine(13,1,13,5);
	dmd.setPixel(14,1);
	dmd.setPixel(15,2);
	dmd.setPixel(14,3);
	dmd.drawLine(15,4,15,5);
	
	dmd.drawLine(17,1,17,5);
	dmd.setPixel(18,1);
	dmd.setPixel(18,5);
	dmd.drawLine(19,2,19,4);
}

// Fungsi untuk menampilkan cursor pilihan stage
void showCursor(int pos)
{
	if((pos == 1) || (pos == 3))
	{
		dmd.setPixel(1,1);
		dmd.setPixel(2,2);
		dmd.setPixel(3,3);
		dmd.setPixel(2,4);
		dmd.setPixel(1,5);
	}
	else if(pos == 2)
	{
		dmd.setPixel(1,8);
		dmd.setPixel(2,9);
		dmd.setPixel(3,10);
		dmd.setPixel(2,11);
		dmd.setPixel(1,12);
	}
}

// Fungsi untuk menampilkan player sesuai playerData
void showPlayer()
{
	dmd.setPixel(playerData.x,(playerData.y)-1);
	dmd.drawLine((playerData.x)-1,playerData.y,(playerData.x)+1,playerData.y);
}

// Fungsi untuk menampilkan semua musuh sesuai enemyArray
void showEnemy()
{
	int type;
	int x;
	int y;
	int i;
	
	for(i<0; i<12; i++)
	{
		if(enemyArray[i].hp > 0)
		{
			type = enemyArray[i].type;
			x = enemyArray[i].x;
			y = enemyArray[i].y;
			
			if(type == 0)
			{
				// Musuh diam
				dmd.setPixel(x,y+1);
				dmd.drawLine(x-1,y,x+1,y);
			}
			else if(type == 1)
			{
				// Musuh bergerak horizontal
				dmd.drawFilledBox(x-1,y,x+1,y+1);
				dmd.setPixel(x,y,GRAPHICS_OFF);
				dmd.setPixel(x,y+2);
			}
			else if(type == 2)
			{
				// Musuh bergerak vertikal
				dmd.drawLine(x-2,y,x-2,y+1);
				dmd.drawLine(x+2,y,x+2,y+1);
				dmd.drawLine(x-1,y+1,x-1,y+2);
				dmd.drawLine(x+1,y+1,x+1,y+2);
				dmd.drawLine(x,y+2,x,y+3);
			}
		}
	}
}

// Fungsi untuk menampilkan layar kemenangan
void showVictory(int animation)
{
	if(animation == 1)
	{
		dmd.drawLine(4,1,6,3);
		dmd.drawLine(3,7,6,7);
		dmd.drawLine(4,13,6,11);
		dmd.drawLine(24,3,26,1);
		dmd.drawLine(24,7,27,7);
		dmd.drawLine(24,11,26,13);
	}
	
	dmd.drawLine(12,3,18,3);
	dmd.drawLine(11,4,12,4);
	dmd.drawLine(18,4,19,4);
	dmd.drawLine(9,5,21,5);
	dmd.drawLine(8,6,8,8);
	dmd.drawLine(22,6,22,8);
	dmd.drawLine(11,6,11,7);
	dmd.drawLine(19,6,19,7);
	dmd.setPixel(12,8);
	dmd.setPixel(18,8);
	dmd.drawLine(9,9,14,9);
	dmd.drawLine(16,9,21,9);
	dmd.drawFilledBox(14,10,16,11);
	dmd.drawFilledBox(12,12,18,13);
}

// Fungsi untuk menampilkan layar kekalahan
void showGameOver(int animation)
{
	if(animation == 1)
	{
		dmd.drawLine(12,5,12,6);
		dmd.drawLine(19,5,19,6);
	}
	
	dmd.drawLine(11,1,20,1);
	dmd.drawLine(8,2,11,2);
	dmd.drawLine(20,2,23,2);
	dmd.setPixel(8,3);
	dmd.setPixel(23,3);
	dmd.drawLine(7,3,7,9);
	dmd.drawLine(24,3,24,9);
	dmd.setPixel(9,9);
	dmd.setPixel(23,9);
	dmd.drawLine(8,10,12,10);
	dmd.drawLine(20,10,23,10);
	dmd.drawLine(12,11,12,12);
	dmd.drawLine(20,11,20,12);
	dmd.drawLine(12,13,20,13);
	dmd.drawLine(14,10,14,12);
	dmd.drawLine(16,10,16,12);
	dmd.drawLine(18,10,18,12);
	
	dmd.drawLine(11,4,13,4);
	dmd.drawFilledBox(10,5,11,6);
	dmd.drawFilledBox(13,5,14,6);
	dmd.drawLine(11,7,13,7);
	
	dmd.drawLine(18,4,20,4);
	dmd.drawFilledBox(17,5,18,6);
	dmd.drawFilledBox(20,5,21,6);
	dmd.drawLine(18,7,20,7);
}

// Fungsi untuk mengenerate stage sesuai difficulty yang dipilih
void generateStage(int difficulty)
{
	int i;
	
	switch(difficulty)
	{
		case 1:
			for(i=0; i<12; i++)
			{
				enemyArray[i].type = 0;
				enemyArray[i].hp = 1;
				enemyArray[i].x = 3 + 5 * (i % 6);
				enemyArray[i].y = 1 + 5 * (i / 6);
			}
			break;
		case 2:
			for(i=0; i<10; i++)
			{
				if(i < 6)
				{
					enemyArray[i].type = 0;
					enemyArray[i].hp = 1;
					enemyArray[i].x = 3 + 5 * (i % 6);
					enemyArray[i].y = 7;
				}
				else
				{
					enemyArray[i].type = 1;
					enemyArray[i].hp = 3;
					enemyArray[i].x = 3 + 8 * (i - 6) + 1 * ((i - 6) / 2);
					enemyArray[i].y = 1;
				}
			}
			break;
			
		case 3:
			for(i=0; i<10; i++)
			{
				if(i < 4)
				{
					enemyArray[i].type = 0;
					enemyArray[i].hp = 1;
					enemyArray[i].x = 3 + 10 * i - 5 * (i / 2);
					enemyArray[i].y = 8;
				}
				else if(i < 8)
				{
					enemyArray[i].type = 1;
					enemyArray[i].hp = 3;
					enemyArray[i].x = 3 + 10 * (i - 4) - 5 * ((i - 4) / 2);
					enemyArray[i].y = 1;
				}
				else
				{
					enemyArray[i].type = 2;
					enemyArray[i].hp = 5;
					enemyArray[i].x = 8 + 15 * (i - 8);
					enemyArray[i].y = 4;
				}
			}
			break;
	}
}

// Fungsi untuk menggerakkan seluruh projectile
void moveProjectile()
{
	int i;
	
	for(i=0; i<20; i++)
	{
		// Cek bila projectile belum expired
		if(projArray[i].valid == 1)
		{
			// Cek untuk projectile musuh
			if(projArray[i].hostile == 1)
			{
				// Gerakkan ke bawah, cek bila menyentuh player
				if(dmd.getPixel(projArray[i].x,(projArray[i].y)+1))
				{
					if(projArray[i].y >= 13)
					{
						// Kurangi HP player
						playerData.hp--;
						projArray[i].valid = 0;
					}
					else
						(projArray[i].y)++;
				}
				else
					(projArray[i].y)++;
				
				// Projectile out of bounds
				if(projArray[i].y > 15)
					projArray[i].valid = 0;
			}
			
			// Cek untuk projectile player
			else // if(projArray[i].hostile == 0)
			{
				// Gerakkan ke bawah, cek bila menyentuh musuh
				if(dmd.getPixel(projArray[i].x,(projArray[i].y)-1))
				{
					// Kurang HP musuh yang disentuh, hilangkan projectile
					hitEnemy(projArray[i].x,projArray[i].y);
					projArray[i].valid = 0;
				}
				else
					(projArray[i].y)--;
				
				// Projectile out of bounds
				if(projArray[i].y < 0)
					projArray[i].valid = 0;
			}
		}
	}
}

// Fungsi untuk menampilkan seluruh projectile
void showProjectile()
{
	int i;
  
	for(i=0; i<20; i++)
	{
		if(projArray[i].valid == 1)
		{
			// Serial.print(i); Serial.print(":\t");
			// Serial.print(projArray[i].x); Serial.print(", ");
			// Serial.println(projArray[i].y);
			dmd.setPixel(projArray[i].x, projArray[i].y);
		}
	}
}

// Fungsi untuk mengurangi HP musuh yang ditembak
void hitEnemy(int x, int y)
{
	int i, j, k;
	
	for(i=-2; i<2; i++)
	{
		for(j=-4; j<0; j++)
		{
			for(k=0; k<12; k++)
			{
				if(enemyArray[k].hp > 0)
				{
					if((enemyArray[k].x == x+i) && (enemyArray[k].y == y+j))
					{
						enemyArray[k].hp--;
           //SFX hit enemy
           song_flag=2;
           if(enemyArray[k].hp==0)
           {
              song_flag=4; 
           }
						break;
					}
				}
			}
		}
	}
 
}

// Fungsi untuk gerakan musuh dan tembakannya
int actEnemy(int animation)
{
	int i;
	int j;
	int retval = 0;
	
	// Serial.println("Act");
	for(i=0; i<12; i++)
	{
		if(enemyArray[i].hp > 0)
		{
			retval = 1;
			switch(enemyArray[i].type)
			{
				case 0:
					// Musuh diam
					break;
				case 1:
					// Musuh bergerak horizontal
					if((animation >= 0) && (animation % 3 == 0))
					{
						if(((((animation/3) % 8) < 2) || (((animation/3) % 8) > 5)))
						{
							// Serial.println("right");
							(enemyArray[i].x)++;
						}
						else
						{
							// Serial.println("left");
							(enemyArray[i].x)--;
						}
					}
					break;
				case 2:
					// Musuh bergerak vertikal
					if((animation >= 0) && ((animation % 3) == 0))
					{
						if(((animation/3) % 24) < 12)
						{
							// Serial.println("down");
							// Bila menabrak player kurangi HP player
							if(dmd.getPixel(enemyArray[i].x,(enemyArray[i].y)+4))
							{
								playerData.hp--;
                //SFX player hit
                song_flag=3;
							}
							(enemyArray[i].y)++;
						}
						else
						{
							// Serial.println("up");
							(enemyArray[i].y)--;
						}
					}
					break;
			}
			
			// RNG untuk tembakan musuh
			if((rand() % 100) == 1) // Kemungkinan 1% untuk setiap pemanggilan fungsi
			{
      //SFX shoot
      song_flag=1;
				// Serial.print(i);
				// Serial.print(": ");
				// Serial.println("Shoot proc");
				for(j=0; j<20; j++)
				{
					if(projArray[j].valid == 0)
					{
						projArray[j].valid = 1;
						projArray[j].hostile = 1;
						projArray[j].x = enemyArray[i].x;
						switch(enemyArray[i].type)
						{
							case 0:
								projArray[j].y = (enemyArray[i].y)+2;
								break;
							case 1:
								projArray[j].y = (enemyArray[i].y)+3;
								break;
							case 2:
								projArray[j].y = (enemyArray[i].y)+4;
								break;
						}
						break;
					}
				}
			}
		}
	}
	
	return retval;
}

//Fungsi untuk membunyikan nada tertentu
void buzz(int targetPin, long frequency, long length)
{
	long delayValue = 1000000 / frequency / 2; // calculate the delay value between transitions
	//// 1 second's worth of microseconds, divided by the frequency, then split in half since
	//// there are two phases to each cycle
	
	long numCycles = frequency * length / 1000; // calculate the number of cycles for proper timing
	//// multiply frequency, which is really cycles per second, by the number of seconds to
	//// get the total number of cycles to produce

	for (long i = 0; i < numCycles; i++)
	{
		// for the calculated length of time...
		digitalWrite(targetPin, HIGH); // write the buzzer pin high to push out the diaphram
		// if((micros() - time) == delayValue)
		delayMicroseconds(delayValue); // wait for the calculated delay value
		digitalWrite(targetPin, LOW); // write the buzzer pin low to pull back the diaphram
		delayMicroseconds(delayValue); // wait again or the calculated delay value
	}
}
