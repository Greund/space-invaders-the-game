#include <Arduino_FreeRTOS.h>
#include <croutine.h>
#include <event_groups.h>
#include <FreeRTOSConfig.h>
#include <FreeRTOSVariant.h>
#include <list.h>
#include <mpu_wrappers.h>
#include <portable.h>
#include <portmacro.h>
#include <projdefs.h>
#include <queue.h>
#include <semphr.h>
#include <StackMacros.h>
#include <task.h>
#include <timers.h>
#include <stdio.h>

#include <avr/io.h>

#define F_CPU 16000000UL
//pin tombol
#define r_button 2
#define l_button 3
#define m_button 4
//data type
// Define types
typedef struct
{
  int type;
  int x;
  int y;
  int hp;
} Ship;
/*************************************************
 * Public Constants
 *************************************************/

#define NOTE_B0  31
#define NOTE_C1  33
#define NOTE_CS1 35
#define NOTE_D1  37
#define NOTE_DS1 39
#define NOTE_E1  41
#define NOTE_F1  44
#define NOTE_FS1 46
#define NOTE_G1  49
#define NOTE_GS1 52
#define NOTE_A1  55
#define NOTE_AS1 58
#define NOTE_B1  62
#define NOTE_C2  65
#define NOTE_CS2 69
#define NOTE_D2  73
#define NOTE_DS2 78
#define NOTE_E2  82
#define NOTE_F2  87
#define NOTE_FS2 93
#define NOTE_G2  98
#define NOTE_GS2 104
#define NOTE_A2  110
#define NOTE_AS2 117
#define NOTE_B2  123
#define NOTE_C3  131
#define NOTE_CS3 139
#define NOTE_D3  147
#define NOTE_DS3 156
#define NOTE_E3  165
#define NOTE_F3  175
#define NOTE_FS3 185
#define NOTE_G3  196
#define NOTE_GS3 208
#define NOTE_A3  220
#define NOTE_AS3 233
#define NOTE_B3  247
#define NOTE_C4  262
#define NOTE_CS4 277
#define NOTE_D4  294
#define NOTE_DS4 311
#define NOTE_E4  330
#define NOTE_F4  349
#define NOTE_FS4 370
#define NOTE_G4  392
#define NOTE_GS4 415
#define NOTE_A4  440
#define NOTE_AS4 466
#define NOTE_B4  494
#define NOTE_C5  523
#define NOTE_CS5 554
#define NOTE_D5  587
#define NOTE_DS5 622
#define NOTE_E5  659
#define NOTE_F5  698
#define NOTE_FS5 740
#define NOTE_G5  784
#define NOTE_GS5 831
#define NOTE_A5  880
#define NOTE_AS5 932
#define NOTE_B5  988
#define NOTE_C6  1047
#define NOTE_CS6 1109
#define NOTE_D6  1175
#define NOTE_DS6 1245
#define NOTE_E6  1319
#define NOTE_F6  1397
#define NOTE_FS6 1480
#define NOTE_G6  1568
#define NOTE_GS6 1661
#define NOTE_A6  1760
#define NOTE_AS6 1865
#define NOTE_B6  1976
#define NOTE_C7  2093
#define NOTE_CS7 2217
#define NOTE_D7  2349
#define NOTE_DS7 2489
#define NOTE_E7  2637
#define NOTE_F7  2794
#define NOTE_FS7 2960
#define NOTE_G7  3136
#define NOTE_GS7 3322
#define NOTE_A7  3520
#define NOTE_AS7 3729
#define NOTE_B7  3951
#define NOTE_C8  4186
#define NOTE_CS8 4435
#define NOTE_D8  4699
#define NOTE_DS8 4978

#define melodyPin 3

void buzz(int targetPin, long frequency, long length);
//Underworld melody
int underworld_melody[] = {
  NOTE_C4, NOTE_C5, NOTE_A3, NOTE_A4,
  NOTE_AS3, NOTE_AS4, 0,
  0,
  NOTE_C4, NOTE_C5, NOTE_A3, NOTE_A4,
  NOTE_AS3, NOTE_AS4, 0,
  0,
  NOTE_F3, NOTE_F4, NOTE_D3, NOTE_D4,
  NOTE_DS3, NOTE_DS4, 0,
  0,
  NOTE_F3, NOTE_F4, NOTE_D3, NOTE_D4,
  NOTE_DS3, NOTE_DS4, 0,
  0, NOTE_DS4, NOTE_CS4, NOTE_D4,
  NOTE_CS4, NOTE_DS4,
  NOTE_DS4, NOTE_GS3,
  NOTE_G3, NOTE_CS4,
  NOTE_C4, NOTE_FS4, NOTE_F4, NOTE_E3, NOTE_AS4, NOTE_A4,
  NOTE_GS4, NOTE_DS4, NOTE_B3,
  NOTE_AS3, NOTE_A3, NOTE_GS3,
  0, 0, 0
};
//Underwolrd tempo
int underworld_tempo[] = {
  12, 12, 12, 12,
  12, 12, 6,
  3,
  12, 12, 12, 12,
  12, 12, 6,
  3,
  12, 12, 12, 12,
  12, 12, 6,
  3,
  12, 12, 12, 12,
  12, 12, 6,
  6, 18, 18, 18,
  6, 6,
  6, 6,
  6, 6,
  18, 18, 18, 18, 18, 18,
  10, 10, 10,
  10, 10, 10,
  3, 3, 3
};
int victory_melody[]={
  NOTE_B5, NOTE_B5, NOTE_B5, NOTE_B5,
  NOTE_B3, NOTE_B4, NOTE_B5, NOTE_B4,
  NOTE_B5
  
};
int victory_tempo[]={
  12, 12, 12, 4,
  4, 4, 6, 12,
  2
};
int edestroy_melody[]={
  NOTE_B5, NOTE_B3
};
int edestroy_tempo[]{
  12, 12
};
int pdestroy_melody[]={
  NOTE_A3, NOTE_A7
};
int pdestroy_tempo[]={
  12, 12
};
//Daftar Task
void vtesSuara(void *pvParameter);
void vTaskBGM(void *pvParameter);
void vTaskSFX(void *pvParameter);
void vtaskButton(void *pvParameter);

//Daftar variabel global
uint8_t s_flag=0;//penanda suara harus dimainkan atau tidak
int move_input; //right = 1, left = -1
int shoot_input;

int gameScreen = 3; // 0 = Title, 1 = stage select, 2 = ingame, 3 = result
int cursorPos = 1; // Stage select cursor position
int victory = 0;
int song_flag=0;//song play choice
Ship enemyArray[18]; // Array containing enemy data
Ship playerData;

int main(void)
{
 
  //Inisialisasi PORT A
  //
  
  pinMode(2,INPUT_PULLUP);//button
   pinMode(3, OUTPUT);//buzzer
  pinMode(13, OUTPUT);//led indicator when singing a note
  //inisialisasi timer
  
  //Buat Task
 xTaskCreate(vtesSuara,"Tes Suara", 85, NULL, 1, NULL);
  xTaskCreate(vTesBGM,"Tes BGM",85,NULL,1,NULL);
  xTaskCreate(vTesSFX, "Tes SFX",85,NULL,1,NULL);

   //Mulai Penjadwal
   vTaskStartScheduler();

   for( ;; );
 }

 void vtesSuara(void *pvParameter)
 {
    TickType_t xLastWakeTime;
    uint8_t count=0;

     xLastWakeTime = xTaskGetTickCount();
     for( ;; )
     {
      gameScreen=2;
        if(digitalRead(2)==0)
        {
        song_flag=4;//shoot
        }
       //Delay
         vTaskDelayUntil( &xLastWakeTime, pdMS_TO_TICKS(90) );
      }
 }
 void vTesBGM(void *pvParameter)
 {
    TickType_t xLastWakeTime;
    uint8_t thisNoteBGM=0;//
    int noteDurBGM;
    
     xLastWakeTime = xTaskGetTickCount();
     for( ;; )
     {
        if(song_flag>=0)//BGM dimainkan saat tidak ada suara lain
        {
          if(gameScreen==2)//bgm tergantung gameScreen
          {
            int len = sizeof(underworld_melody) / sizeof(int);
            while((song_flag>=0)&&(thisNoteBGM<=len)) 
            {
              if(thisNoteBGM<len)
              {
                // to calculate the note duration, take one second
                // divided by the note type.
                //e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
                noteDurBGM = 1000 / underworld_tempo[thisNoteBGM];
                taskENTER_CRITICAL();
            
                buzz(melodyPin, underworld_melody[thisNoteBGM], noteDurBGM);
                taskEXIT_CRITICAL();
                // to distinguish the notes, set a minimum time between them.
                // the note's duration + 30% seems to work well:
                int pauseBetweenNotes =( noteDurBGM * 1.30)/15;
                //Delay
                for(int pauseCount=0;pauseCount<pauseBetweenNotes;pauseCount++)
                {
                  vTaskDelayUntil( &xLastWakeTime, pdMS_TO_TICKS(30) );
                }
                // stop the tone playing:
                taskENTER_CRITICAL();
                buzz(melodyPin, 0, noteDurBGM);
                taskEXIT_CRITICAL();
            thisNoteBGM++;
            
               
              }
              else
              {
                thisNoteBGM=0;
              }
            }
          }
        }
      }
 }
 void vTesSFX(void *pvParameter)
 {
    TickType_t xLastWakeTime;
    uint8_t thisNote=0;//
     uint8_t len;
    int noteDur;
    
     xLastWakeTime = xTaskGetTickCount();
     for( ;; )
     {
        if(song_flag==1)//shoot
    {
      // to calculate the note duration, take one second
      // divided by the note type.
      //e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
      noteDur = 1000 / 8;
      taskENTER_CRITICAL();
      
     buzz(melodyPin, NOTE_E7, noteDur);
     taskEXIT_CRITICAL();
      song_flag=0;
            
    }
    else if(song_flag==2)//hit enemy
    {
      // to calculate the note duration, take one second
      // divided by the note type.
      //e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
      noteDur = 1000 / 8;
      taskENTER_CRITICAL();
      
      buzz(melodyPin, NOTE_D5, noteDur);
      taskEXIT_CRITICAL();
      song_flag=0;
    }
    else if(song_flag==3)//hit player
    {
      // to calculate the note duration, take one second
      // divided by the note type.
      //e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
      noteDur = 1000 / 8;
      taskENTER_CRITICAL();
      
      buzz(melodyPin, NOTE_F5, noteDur);
      taskEXIT_CRITICAL();
      song_flag=0;
    }
    else if(song_flag==4)//enemy destroyed
    {
      len=sizeof(edestroy_melody) / sizeof(int);
      // to calculate the note duration, take one second
      // divided by the note type.
      //e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
      noteDur = 1000 / edestroy_tempo[thisNote];
      taskENTER_CRITICAL();
      
    buzz(melodyPin, edestroy_melody[thisNote], noteDur);
      taskEXIT_CRITICAL();
      if(thisNote<len)
      {
        thisNote++;
      }
      else
      {
        thisNote=0;
        song_flag=0;
      }
    }
    else if(song_flag==5)//enemy destroyed
    {
      len=sizeof(pdestroy_melody) / sizeof(int);
      // to calculate the note duration, take one second
      // divided by the note type.
      //e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
      noteDur = 1000 / pdestroy_tempo[thisNote];
      taskENTER_CRITICAL();
      
    buzz(melodyPin, pdestroy_melody[thisNote], noteDur);
      taskEXIT_CRITICAL();
      if(thisNote<len)
      {
        thisNote++;
      }
      else
      {
        thisNote=0;
        song_flag=0;
      }
    }
    int pauseBetweenNotes = ( noteDur* 1.30)/20;
      //delay
        for(int pauseCount=0;pauseCount<=pauseBetweenNotes;pauseCount++)
                {
                  vTaskDelayUntil( &xLastWakeTime, pdMS_TO_TICKS(25) );
                }
                // stop the tone playing:
               
     }
 }
void vtesButton(void *pvParameter)
{
  TickType_t xLastWakeTime;
  uint8_t left, right, mid;
  xLastWakeTime = xTaskGetTickCount();
  for( ; ; )
  {
    //membaca input tombol ke variabel
    left=digitalRead(l_button);
    right=digitalRead(r_button);
    mid=digitalRead(m_button);
    //translasi input berdasarkan mode
    //Title Screen
    if(gameScreen==0)
    {
      //mid button untuk memulai permainan
      if(mid==1)
      {
        shoot_input=1;
      }
      if(right||left)
      {
         move_input=1;  
      }
    }
    else 
    {
      //right button untuk bergerak ke kanan
      if(right==1)
      {
        move_input=1;
      }
      //left button untuk bergerak ke kiri
      if(left==1)
      {
        move_input=-1;
      }
      //mid button untuk menembak atau memilih
      if(mid==1)
      {
        shoot_input=1;
      }
    }
    //Delay
        vTaskDelayUntil( &xLastWakeTime, pdMS_TO_TICKS(100) );
  }
}
 void loop(){
 }
 
void buzz(int targetPin, long frequency, long length) {
  digitalWrite(13, HIGH);
  long delayValue = 1000000 / frequency / 2; // calculate the delay value between transitions
  //// 1 second's worth of microseconds, divided by the frequency, then split in half since
  //// there are two phases to each cycle
  long numCycles = frequency * length / 1000; // calculate the number of cycles for proper timing
  //// multiply frequency, which is really cycles per second, by the number of seconds to
  //// get the total number of cycles to produce
  for (long i = 0; i < numCycles; i++) { // for the calculated length of time...
    digitalWrite(targetPin, HIGH); // write the buzzer pin high to push out the diaphram
    delayMicroseconds(delayValue); // wait for the calculated delay value
    digitalWrite(targetPin, LOW); // write the buzzer pin low to pull back the diaphram
    delayMicroseconds(delayValue); // wait again or the calculated delay value
  }
  digitalWrite(13, LOW);

}

