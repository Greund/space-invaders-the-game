//http://pixelartmaker.com/art/d5c66eb6972269e

#include <Arduino_FreeRTOS.h>

#include <SPI.h>
#include <DMD2.h>

#include <time.h>
#include <stdlib.h>

// Define types
typedef struct
{
	int type; // Enemy types
	int x;
	int y;
	int hp; // Hit points
} Ship;

typedef struct
{
	int hostile; // 1 = hostile (shot by the enemy)
	int x;
	int y;
	int valid;
} Projectile;

// Define tasks
void TaskGame(void *pvParameters);
void TaskDisplay(void *pvParameters);

SoftDMD dmd(1,1);  // DMD controls the entire display

// Global variables
int move_input; //right = 1, left = -1
int shoot_input = 1;

int gameScreen = 0; // 0 = Title, 1 = stage select, 2 = ingame, 3 = result
int cursorPos = 3; // Stage select cursor position
int victory = 0;

Ship enemyArray[12]; // Array containing enemy data
Ship playerData;
Projectile projArray[20]; // Array containing all projectile data

// the setup function runs once when you press reset or power the board
void setup() {
	// initialize serial communication at 9600 bits per second:
	Serial.begin(9600);
	dmd.setBrightness(1);
	dmd.begin();
	// dmd.beginNoTimer();
	srand((unsigned int)time(NULL)); // Random function seed

	xTaskCreate(TaskDisplay, (const portCHAR *)"Display", 128, NULL, 1, NULL);
	xTaskCreate(TaskGame, (const portCHAR *)"Game", 128, NULL, 1, NULL);

	// Now the task scheduler, which takes over control of scheduling individual tasks, is automatically started.
}

void loop()
{
	// Empty. Things are done in Tasks.
}

/*--------------------------------------------------*/
/*---------------------- Tasks ---------------------*/
/*--------------------------------------------------*/

void TaskGame(void *pvParameters)
{
	(void) pvParameters;
	TickType_t xLastWakeTime;
	int i;
	int counter = -5;
	
	xLastWakeTime = xTaskGetTickCount();
	for(;;)
	{		
		if(gameScreen == 0)
		{
			if(move_input || shoot_input)
				gameScreen = 1;
		}
		else if(gameScreen == 1)
		{
			cursorPos += move_input;
			if(cursorPos < 0)
				cursorPos = 0;
			if(cursorPos > 4)
				cursorPos = 4;
			
			if(shoot_input)
			{
				playerData.type = 0;
				playerData.hp = 3;
				playerData.x = 16;
				playerData.y = 15;
				generateStage(cursorPos);
				gameScreen = 2;
			}
		}
		else if(gameScreen == 2)
		{
			playerData.x += move_input;
			
			if(0)//shoot_input)
			{
				for(i=0; i<100; i++)
				{
					if(projArray[i].valid == 0)
					{
						projArray[i].hostile = 0;
						projArray[i].x = playerData.x;
						projArray[i].y = (playerData.y)-2;
						projArray[i].valid = 1;
						break;
					}
				}
			}
			
			moveProjectile();
			if(playerData.hp == 0)
			{
				//gameScreen = 3;
				victory = 0;
			}
			else
			{
				if(actEnemy(counter) == 0)
				{
					gameScreen = 3;
					victory = 1;
				}
			}
		}
		else if(gameScreen == 3)
		{	
			if(shoot_input)
				gameScreen = 0;
		}

		Serial.println(counter);
		counter++;
		if(counter > 352)
			counter = 0;
		
		// Periode task 100 ms
		vTaskDelayUntil( &xLastWakeTime, pdMS_TO_TICKS(150) );
	}
}

void TaskDisplay(void *pvParameters)
{
	(void) pvParameters;
	TickType_t xLastWakeTime;
	int x = 0;
	int counter = 0;
	
	xLastWakeTime = xTaskGetTickCount();
	for(;;)
	{
		dmd.clearScreen();
		if(gameScreen == 0)
		{
			// Display title screen
			titleScreen();
		}
		else if(gameScreen == 1)
		{
			// Display stage selection screen
			showCursor(cursorPos);
			
			if((cursorPos == 1) || (cursorPos == 2))
				stageSelect1();
			
			else if((cursorPos == 3) || (cursorPos == 4))
				stageSelect2();
		}
		else if(gameScreen == 2)
		{
			// Display ingame screen
			showPlayer();
			showEnemy();
			showProjectile();
		}
		else if(gameScreen == 3)
		{
			//Display results screen
			if(victory == 1)
				showVictory(counter/5);
			
			else
				showGameOver(counter/5);
		}
		
		counter++;
		if(counter == 10)
			counter = 0;

		// dmd.scanDisplay();
		// Periode task 100 ms
		vTaskDelayUntil( &xLastWakeTime, pdMS_TO_TICKS(5) );
	}
}

void titleScreen()
{
	dmd.drawLine(1,1,3,1);
	dmd.drawLine(1,2,3,4);
	dmd.drawLine(1,5,3,5);

	dmd.drawLine(5,1,5,5);
	dmd.setPixel(6,1);
	dmd.setPixel(6,3);
	dmd.drawLine(7,1,7,3);

	dmd.drawLine(9,1,9,5);
	dmd.setPixel(10,1);
	dmd.setPixel(10,3);
	dmd.drawLine(11,1,11,5);
	
	dmd.drawLine(13,1,13,5);
	dmd.drawLine(14,1,15,1);
	dmd.drawLine(14,5,15,5);
	
	dmd.drawLine(17,1,17,5);
	dmd.drawLine(18,1,19,1);
	dmd.drawLine(18,3,19,3);
	dmd.drawLine(18,5,19,5);
	
	dmd.drawLine(3,7,5,7);
	dmd.drawLine(4,8,4,10);
	dmd.drawLine(3,11,5,11);
	
	dmd.drawLine(7,7,7,11);
	dmd.setPixel(8,7);
	dmd.drawLine(9,7,9,11);
	
	dmd.drawLine(11,7,11,10);
	dmd.drawLine(12,10,12,11);
	dmd.drawLine(13,7,13,10);
	
	dmd.drawLine(15,7,15,11);
	dmd.setPixel(16,7);
	dmd.setPixel(16,9);
	dmd.drawLine(17,7,17,11);
	
	dmd.drawLine(19,7,19,11);
	dmd.setPixel(20,7);
	dmd.setPixel(20,11);
	dmd.drawLine(21,8,21,10);
	
	dmd.drawLine(23,7,23,11);
	dmd.drawLine(24,7,25,7);
	dmd.drawLine(24,9,25,9);
	dmd.drawLine(24,11,25,11);
	
	dmd.drawLine(27,7,27,11);
	dmd.setPixel(28,7);
	dmd.setPixel(29,8);
	dmd.setPixel(28,9);
	dmd.drawLine(29,10,29,11);
}

void stageSelect1()
{
	dmd.drawLine(5,1,5,5);
	dmd.drawLine(6,1,7,1);
	dmd.drawLine(6,3,7,3);
	dmd.drawLine(6,5,7,5);
	
	dmd.drawLine(9,1,9,5);
	dmd.setPixel(10,1);
	dmd.setPixel(10,3);
	dmd.drawLine(11,1,11,5);
	
	dmd.drawLine(13,1,15,1);
	dmd.drawLine(13,2,15,4);
	dmd.drawLine(13,5,15,5);
	
	dmd.drawLine(17,1,17,2);
	dmd.drawLine(19,1,19,2);
	dmd.drawLine(18,3,18,5);
	
	dmd.drawLine(5,8,5,12);
	dmd.setPixel(6,8);
	dmd.drawLine(7,8,7,12);
	
	dmd.drawLine(9,8,9,12);
	dmd.setPixel(10,8);
	dmd.setPixel(10,12);
	dmd.drawLine(11,8,11,12);
	
	dmd.drawLine(13,8,13,12);
	dmd.setPixel(14,8);
	dmd.setPixel(15,9);
	dmd.setPixel(14,10);
	dmd.drawLine(15,11,15,12);
	
	dmd.drawLine(17,8,17,12);
	dmd.setPixel(18,8);
	dmd.drawLine(19,9,19,12);
	dmd.setPixel(20,8);
	dmd.drawLine(21,8,21,12);
	
	dmd.drawLine(23,8,23,12);
	dmd.setPixel(24,8);
	dmd.setPixel(24,10);
	dmd.drawLine(25,8,25,12);
	
	dmd.drawLine(27,8,27,12);
	dmd.drawLine(28,12,29,12);
}

void stageSelect2()
{
	dmd.drawLine(5,1,5,5);
	dmd.setPixel(6,3);
	dmd.drawLine(7,1,7,5);
	
	dmd.drawLine(9,1,9,5);
	dmd.setPixel(10,1);
	dmd.setPixel(10,3);
	dmd.drawLine(11,1,11,5);
	
	dmd.drawLine(13,1,13,5);
	dmd.setPixel(14,1);
	dmd.setPixel(15,2);
	dmd.setPixel(14,3);
	dmd.drawLine(15,4,15,5);
	
	dmd.drawLine(17,1,17,5);
	dmd.setPixel(18,1);
	dmd.setPixel(18,5);
	dmd.drawLine(19,2,19,4);
	
	dmd.drawLine(5,8,5,12);
	dmd.setPixel(6,8);
	dmd.drawLine(7,9,7,12);
	dmd.setPixel(8,8);
	dmd.drawLine(9,8,9,12);
	
	dmd.drawLine(11,8,11,12);
	dmd.setPixel(12,8);
	dmd.setPixel(12,10);
	dmd.drawLine(13,8,13,12);
	
	dmd.drawLine(15,8,15,12);
	dmd.setPixel(16,8);
	dmd.drawLine(17,8,17,12);
	
	dmd.drawLine(19,8,21,8);
	dmd.drawLine(20,9,20,11);
	dmd.drawLine(19,12,21,12);
	
	dmd.drawLine(23,8,23,12);
	dmd.setPixel(24,8);
	dmd.setPixel(24,10);
	dmd.drawLine(25,8,25,12);
	
	dmd.drawLine(27,8,27,12);
	dmd.drawLine(28,8,29,8);
	dmd.drawLine(28,12,29,12);
}

void showCursor(int pos)
{
	if((pos == 1) || (pos == 3))
	{
		dmd.setPixel(1,1);
		dmd.setPixel(2,2);
		dmd.setPixel(3,3);
		dmd.setPixel(2,4);
		dmd.setPixel(1,5);
	}
	else if((pos == 2) || (pos == 4))
	{
		dmd.setPixel(1,8);
		dmd.setPixel(2,9);
		dmd.setPixel(3,10);
		dmd.setPixel(2,11);
		dmd.setPixel(1,12);
	}
}

void showPlayer()
{
	dmd.setPixel(playerData.x,(playerData.y)-1);
	dmd.drawLine((playerData.x)-1,playerData.y,(playerData.x)+1,playerData.y);
}

void showEnemy()
{
	int type;
	int x;
	int y;
	int i;
	
	for(i<0; i<12; i++)
	{
		if(enemyArray[i].hp > 0)
		{
			type = enemyArray[i].type;
			x = enemyArray[i].x;
			y = enemyArray[i].y;
			
			if(type == 0)
			{
				dmd.setPixel(x,y+1);
				dmd.drawLine(x-1,y,x+1,y);
			}
			else if(type == 1)
			{
				dmd.drawFilledBox(x-1,y,x+1,y+1);
				dmd.setPixel(x,y,GRAPHICS_OFF);
				dmd.setPixel(x,y+2);
			}
			else if(type == 2)
			{
				dmd.drawLine(x-2,y,x-2,y+1);
				dmd.drawLine(x+2,y,x+2,y+1);
				dmd.drawLine(x-1,y+1,x-1,y+2);
				dmd.drawLine(x+1,y+1,x+1,y+2);
				dmd.drawLine(x,y+2,x,y+3);
			}
			else if(type == 3)
			{
				dmd.drawLine(x-8,y,x-3,y);
				dmd.drawLine(x+3,y,x+8,y);
				dmd.setPixel(x-6,y+1);
				dmd.setPixel(x+6,y+1);
				dmd.drawLine(x-3,y+1,x+3,y+1);
				dmd.drawFilledBox(x-2,y+2,x+2,y+4);
				dmd.drawLine(x-1,y+5,x+1,y+5);
				dmd.setPixel(x-3,y+5);
				dmd.setPixel(x+3,y+5);
				dmd.setPixel(x,y+6);
			}
		}
	}
}

void showVictory(int animation)
{
	if(animation == 1)
	{
		dmd.drawLine(4,1,6,3);
		dmd.drawLine(3,7,6,7);
		dmd.drawLine(4,13,6,11);
		dmd.drawLine(24,3,26,1);
		dmd.drawLine(24,7,27,7);
		dmd.drawLine(24,11,26,13);
	}
	
	dmd.drawLine(12,3,18,3);
	dmd.drawLine(11,4,12,4);
	dmd.drawLine(18,4,19,4);
	dmd.drawLine(9,5,21,5);
	dmd.drawLine(8,6,8,8);
	dmd.drawLine(22,6,22,8);
	dmd.drawLine(11,6,11,7);
	dmd.drawLine(19,6,19,7);
	dmd.setPixel(12,8);
	dmd.setPixel(18,8);
	dmd.drawLine(9,9,14,9);
	dmd.drawLine(16,9,21,9);
	dmd.drawFilledBox(14,10,16,11);
	dmd.drawFilledBox(12,12,18,13);
}

void showGameOver(int animation)
{
	if(animation == 1)
	{
		dmd.drawLine(12,5,12,6);
		dmd.drawLine(19,5,19,6);
	}
	
	dmd.drawLine(11,1,20,1);
	dmd.drawLine(8,2,11,2);
	dmd.drawLine(20,2,23,2);
	dmd.setPixel(8,3);
	dmd.setPixel(23,3);
	dmd.drawLine(7,3,7,9);
	dmd.drawLine(24,3,24,9);
	dmd.setPixel(9,9);
	dmd.setPixel(23,9);
	dmd.drawLine(8,10,12,10);
	dmd.drawLine(20,10,23,10);
	dmd.drawLine(12,11,12,12);
	dmd.drawLine(20,11,20,12);
	dmd.drawLine(12,13,20,13);
	dmd.drawLine(14,10,14,12);
	dmd.drawLine(16,10,16,12);
	dmd.drawLine(18,10,18,12);
	
	dmd.drawLine(11,4,13,4);
	dmd.drawFilledBox(10,5,11,6);
	dmd.drawFilledBox(13,5,14,6);
	dmd.drawLine(11,7,13,7);
	
	dmd.drawLine(18,4,20,4);
	dmd.drawFilledBox(17,5,18,6);
	dmd.drawFilledBox(20,5,21,6);
	dmd.drawLine(18,7,20,7);
}

void generateStage(int difficulty)
{
	int i;
	
	switch(difficulty)
	{
		case 1:
			for(i=0; i<12; i++)
			{
				enemyArray[i].type = 0;
				enemyArray[i].hp = 1;
				enemyArray[i].x = 3 + 5 * (i % 6);
				enemyArray[i].y = 1 + 5 * (i / 6);
			}
			break;
		case 2:
			for(i=6; i<10; i++)
			{
				if(i < 6)
				{
					enemyArray[i].type = 0;
					enemyArray[i].hp = 1;
					enemyArray[i].x = 3 + 5 * (i % 6);
					enemyArray[i].y = 7;
				}
				else
				{
					enemyArray[i].type = 1;
					enemyArray[i].hp = 3;
					enemyArray[i].x = 3 + 8 * (i - 6) + 1 * ((i - 6) / 2);
					enemyArray[i].y = 1;
				}
			}
			break;
			
		case 3:
			for(i=0; i<10; i++)
			{
				if(i < 4)
				{
					enemyArray[i].type = 0;
					enemyArray[i].hp = 1;
					enemyArray[i].x = 3 + 10 * i - 5 * (i / 2);
					enemyArray[i].y = 8;
				}
				else if(i < 8)
				{
					enemyArray[i].type = 1;
					enemyArray[i].hp = 3;
					enemyArray[i].x = 3 + 10 * (i - 4) - 5 * ((i - 4) / 2);
					enemyArray[i].y = 1;
				}
				else
				{
					enemyArray[i].type = 2;
					enemyArray[i].hp = 5;
					enemyArray[i].x = 8 + 15 * (i - 8);
					enemyArray[i].y = 4;
				}
			}
			break;
			
		case 4:
			enemyArray[0].type = 3;
			enemyArray[0].hp = 10;
			enemyArray[0].x = 16;
			enemyArray[0].y = 3;
			break;
	}
}

void moveProjectile()
{
	int i;
	
	for(i=0; i<20; i++)
	{
		if(projArray[i].valid == 1)
		{
			if(projArray[i].hostile == 1)
			{
				if(dmd.getPixel(projArray[i].x,(projArray[i].y)+1))
				{
					if(projArray[i].y >= 13)
					{
						playerData.hp--;
						projArray[i].valid = 0;
					}
					else
						(projArray[i].y)++;
				}
				else
					(projArray[i].y)++;
				
				if(projArray[i].y > 15)
					projArray[i].valid = 0;
			}
			else // if(projArray[i].hostile == 0)
			{
				if(dmd.getPixel(projArray[i].x,(projArray[i].y)-1))
				{
					hitEnemy(projArray[i].x,projArray[i].y);
					projArray[i].valid = 0;
				}
				else
					(projArray[i].y)--;
				
				if(projArray[i].y < 0)
					projArray[i].valid = 0;
			}
		}
	}
}

void showProjectile()
{
	int i;
  
	for(i=0; i<20; i++)
	{
		if(projArray[i].valid == 1)
		{
			// Serial.print(i); Serial.print(":\t");
			// Serial.print(projArray[i].x); Serial.print(", ");
			// Serial.println(projArray[i].y);
			dmd.setPixel(projArray[i].x, projArray[i].y);
		}
	}
}

void hitEnemy(int x, int y)
{
	int i, j, k;
	
	for(i=-2; i<2; i++)
	{
		for(j=-2; j<2; j++)
		{
			for(k=0; k<12; k++)
			{
				if(enemyArray[k].hp > 0)
				{
					if((enemyArray[k].x == x+i) && (enemyArray[k].y == y+j))
					{
						enemyArray[k].hp--;
						break;
					}
				}
			}
		}
	}
}

int actEnemy(int animation)
{
	int i;
	int j;
	int retval = 0;
	
	for(i=0; i<12; i++)
	{
		if(enemyArray[i].hp > 0)
		{
			retval = 1;
			switch(enemyArray[i].type)
			{
				case 0:
					break;
				case 1:
					// Serial.println(animation % 7);
					if(animation >= 0)
					{
						if((((animation % 8) < 2) || ((animation % 8) > 5)))
						{
							// Serial.println("right");
							(enemyArray[i].x)++;
						}
						else
						{
							// Serial.println("left");
							(enemyArray[i].x)--;
						}
					}
					break;
				case 2:
					if(!(((animation % 44) >= 4) && ((animation % 44) <= 10)))
					{
						if((((animation % 44) < 4) || ((animation % 44) > 29)))
							(enemyArray[i].y)--;
						else
							(enemyArray[i].y)++;
					}
					break;
				case 3:
					break;
			}
			
			// Shoot
			if((rand() % 10) == 1)
			{
				// Serial.print(i);
				// Serial.print(": ");
				// Serial.println("Shoot proc");
				for(j=0; j<20; j++)
				{
					if(projArray[j].valid == 0)
					{
						projArray[j].valid = 1;
						projArray[j].hostile = 1;
						projArray[j].x = enemyArray[i].x;
						switch(enemyArray[i].type)
						{
							case 0:
								projArray[j].y = (enemyArray[j].y)+2;
								break;
							case 1:
								projArray[j].y = (enemyArray[j].y)+3;
								break;
							case 2:
								projArray[j].y = (enemyArray[j].y)+4;
								break;
							case 3:
								projArray[j].y = (enemyArray[j].y)+7;
								break;
						}
						break;
					}
				}
			}
		}
	}
	
	return retval;
}
